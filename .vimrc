" Plugins
call plug#begin()

Plug 'drewtempelmeyer/palenight.vim'

call plug#end()


"Display
set number "Line numbers
set background=dark
colorscheme palenight "Uses the Plug package manager
syntax enable
set tabstop=3 "How vim displays tabs
set softtabstop=3 "How vim inserts tabs
set expandtab "Set tabs to spaces
set shiftwidth=3 "Amount to autoindent
set autoindent "Turn it on
set smartindent "No idea what this does
set cursorline "Highlight current line
set cursorcolumn "Vertical line
set wildmenu "Visual autocomplete for vim commands
set lazyredraw "Only redraw when you need to
set showmatch "Hihglight pairing parenthesis
set hlsearch "Highlight search matches
set foldenable "Enable code folding
set foldmethod=indent
set foldlevelstart=10 "Arbitrary
set guifont=Hack\ 12
set hlsearch "Highlight all instances of word under cursor using shift+*
au BufNewFile,BufRead,BufReadPost *.sv set syntax=verilog

"Detect unwanted whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
