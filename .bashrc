##Aliases
alias ll='ls -la'
alias extract='tar -xvf'
alias compilec='make program SOURCE='
alias compileas='make assembly SOURCE='

##Do an ll when changing directories
cd() {
   builtin cd "$@" && ll
}

##Highlight grep matches
export GREP_OPTIONS='--color=auto'

##ls colors
##Directories are bold yellow, .v files are regular cyan, .vg files are regular purple
export LS_COLORS="di=1;33:*.v=0;36:*.vg=0;35"

##Prompt colors from The Nathan Brown
PS1='\[\033[0;36m\]\w\[\033[0;32m\]$\033[0;32m\]\$ \[\033[0m\]'

##Load the 312 environment
module load eecs312/w19

##Enable 470 RISC Makefile
export HOME=/home/cknebel
export PATH=$PATH:~/EECS470/riscv64-unknown-elf/bin
export PATH=$PATH:~/EECS470/riscv64-unknown-elf/bin:$RISCV/bin
export RISCV=~/EEECS470/riscv64-unknown-elf
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/EEECS470/riscv64-unknown-elf

##Update git version
module load git
